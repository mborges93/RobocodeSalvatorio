package fatec2018;
import robocode.*;
import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * Hulk - a robot by (your name here)
 */
public class Aniquilador extends Robot{


	public void run() {
		setBodyColor(Color.blue);
		setGunColor(Color.blue);
		setRadarColor(Color.blue);
		

     boolean valor0 = true;
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		// setColors(Color.red,Color.blue,Color.green); // body,gun,radar

		// Robot main loop
		while(true) {
			// Replace the next 4 lines with any behavior you would like
			
            if(valor0 == true){
			for(int valor1=0;valor1<3;valor1++){
			ahead(150);
			turnRight(90);
			turnGunLeft(360);
			
			}
			for(int valor=3;valor>0;valor--){
			ahead(150);
			turnLeft(90);
			turnGunRight(360);
			}
			valor0 = false;
			
			}else{
			for(int valor=3;valor>0;valor--){
			ahead(180);
			turnLeft(120);
			turnGunRight(360);
				
			}
			
			for(int valor1=0;valor1<3;valor1++){
			ahead(180);
			turnRight(120);
			turnGunLeft(360);
			
        }
		
		
		  valor0 = true;
		
		}
			
			
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		fire(3);
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
//	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		//back(10);
	//}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(20);
	}	
}
