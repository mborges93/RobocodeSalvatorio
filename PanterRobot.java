package fatec2018;

import robocode.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;
import java.awt.*;

public class PanterRobot extends AdvancedRobot {
	boolean movingForward; // e definida como true quando setAhead a chamada e vice-versa
	boolean inWall; // e verdade quando robp esta perto da parede.

	public void run() {

		setColors();

		// Cada parte do robo move-se livremente dos outros.
		setAdjustRadarForRobotTurn(true);
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);

		// Verifique se o robp esta mais perto do que 50px da parede.
		if (getX() <= 50 || getY() <= 50
				|| getBattleFieldWidth() - getX() <= 50
				|| getBattleFieldHeight() - getY() <= 50) {
			this.inWall = true;
		} else {
			this.inWall = false;
		}

		setAhead(40000); // va em frente ate inverter o sentido
		setTurnRadarRight(360); // scannear ate encontrar seu primeiro inimigo
		this.movingForward = true; // chamamos setAhead, entao movingForward  verdade

		while (true) {
			// Verifica se estamos perto da parede e se ja verificamos positivo.
			// Caso nao verificamos, inverte a direcao e seta 
			// para true.
			if (getX() > 50 && getY() > 50
					&& getBattleFieldWidth() - getX() > 50
					&& getBattleFieldHeight() - getY() > 50
					&& this.inWall == true) {
				this.inWall = false;
			}
			if (getX() <= 50 || getY() <= 50
					|| getBattleFieldWidth() - getX() <= 50
					|| getBattleFieldHeight() - getY() <= 50) {
				if (this.inWall == false) {
					reverseDirection();
					inWall = true;
				}
			}

			// Se o radar parou de girar, procure um inimigo
			if (getRadarTurnRemaining() == 0.0) {
				setTurnRadarRight(360);
			}

			execute();
		}
	}

	public void onHitWall(HitWallEvent e) {
		reverseDirection();
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		// Calcular a posicao exata do robo
		double absoluteBearing = getHeading() + e.getBearing();

		// vire so o necessario e nunca mais do que uma volta...
		// vendo-se o angulo que fazemos com o robo alvo e descontando
		// o Heading e o Heading do Radar pra ficar com o angulo
		// correto, normalmente.
		double bearingFromGun = normalRelativeAngleDegrees(absoluteBearing
				- getGunHeading());
		double bearingFromRadar = normalRelativeAngleDegrees(absoluteBearing
				- getRadarHeading());

		// giro para realizar movimento espiral ao inimigo
		// (90 levaria ao paralelo)
		if (this.movingForward) {
			setTurnRight(normalRelativeAngleDegrees(e.getBearing() + 80));
		} else {
			setTurnRight(normalRelativeAngleDegrees(e.getBearing() + 100));
		}

		// Se perto o suficiente, atire
		if (Math.abs(bearingFromGun) <= 4) {
			setTurnGunRight(bearingFromGun); // mantem o canhao centrado sobre o inimigo
			setTurnRadarRight(bearingFromRadar); // mantem o radar centrado sobre o inimigo

			// Quanto mais precisamente objetivo, maior sera a bala.
			// Nao dispare nos a deficiencia, sempre salvar 0,1
			if (getGunHeat() == 0 && getEnergy() > .2) {
				fire(Math.min(
						4.5 - Math.abs(bearingFromGun) / 2 - e.getDistance() / 250, 
						getEnergy() - .1));
			}
		} // caso contr�rio, basta definir a arma para virar.
		else {
			setTurnGunRight(bearingFromGun);
			setTurnRadarRight(bearingFromRadar);
		}

		// se o radar estiver parado, girar (scanner)
		if (bearingFromGun == 0) {
			scan();
		}
	}

	// Se batermos em um robo, voltar
	public void onHitRobot(HitRobotEvent e) {
		if (e.isMyFault()) {
			reverseDirection();
		}
	}

	private void setColors() {
		setBodyColor(Color.BLUE);
		setGunColor(Color.BLUE);
		setRadarColor(Color.BLUE);
		setBulletColor(Color.GRAY);
		setScanColor(Color.GRAY);
	}

	// mudar para frente / tras
	public void reverseDirection() {
		if (this.movingForward) {
			setBack(40000);
			this.movingForward = false;
		} else {
			setAhead(40000);
			this.movingForward = true;
		}
	}
}