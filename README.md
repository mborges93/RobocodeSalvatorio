<h2>Trabalho desenvolvido para cadeira de Engenharia de Software 2</h2>

<h3>Engenheiros de Software</h3>
Matheus Cardoso Borges -
Lucas Bergmann

<h3>Issues e Milestones</h3>
*Serão criadas Milestones com prazos de 1 semana com Issues compostas de tarefas direcionadas a tal desenvolvedor, com peso para cada respectiva tarefa.

*O desenvolvedor deve respeitar rigorosamente o prazo estipulado para o cumprimento da tarefa designada, caso descumpra podera ser desligado do projeto a qualquer momento.

<h3>Documentação Explicação</h3>
[Site Oficial Robocode](http://robocode.sourceforge.net/)

[Readme Robocode](http://robocode.sourceforge.net/docs/ReadMe.html)

[Como instalar, Primeiros Passos e Principais Comandos](http://www3.ifrn.edu.br/~andrealmeida/lib/exe/fetch.php?media=aulas:2011.1:poo:aula09robocode.pdf)

*Após Estudos dos Links acima, os desenvolvedores estarão com aptidao para começarem a desenvolverem os robos da equipe.

<h3>Mais links de Ajuda</h3>
[Por onde Começar](https://www.ft.unicamp.br/liag/robocode/robocode/por-onde-comecar/)

[Batalhando e apendendo com robocode](https://www.ft.unicamp.br/liag/wp-content/uploads/2016/03/Robocode.pdf)

[Radar](http://old.robowiki.net/robowiki?Radar/Old)

[Manual](http://www.ufjf.br/jairo_souza/files/2015/11/Robocode-Manual-de-Instruções.pdf)

[Tutorial como manter outro robo na mira](https://www.portugal-a-programar.pt/forums/topic/11220-tutorial-manter-outro-robot-na-mira/)

<h3>Estratégia</h3>
[Link Estratégia](https://docs.google.com/document/d/1ZCaB-Z6BIwXpcMaf51ziAqR4zzUywsKAUQbrTPQiGHo/edit)



<h1>Duvidas / Dicas</h1>
[Click Aqui](https://gitlab.com/mborges93/RobocodeSalvatorio/issues/4)